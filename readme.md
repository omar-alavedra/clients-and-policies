# PROJECT CLIENTS AND POLICIES
by Omar Alavedra.

## Brief introduction
This project is a ResfulAPI based on a backend code assessment. 
Is made with Node.js and the reason why is because of the great efficiency, the high performance, 
modularity and also scalable for the future.
The main duty of this ResfulAPI is to expose the following services with their inherent restrictions:
*   Offer a Login for the users with authentification.
*   Get user data filtered by user id -> Can be accessed by users with role "user" or "admin"
*   Get user data filtered by user name -> Can be accessed by users with role "user" or "admin"
*   Get the list of pilicies linked to a user nme -> Can be accessed by users with role "admin"
*   Get the user linked to a policy number -> Can be accessed by users with role "admin"


I hope the you enjoy interacting with Clients and Policies API, I accept all kinds of suggestions for improvement. Thank you very much for the opportunity.

## Improvements for the future
Based on the time of the development (One and half days), there are some improvements to do in the near future like:
* Improve an interface for Login/Logout and SignUp for new users.
* Add even more testing.
* Other bugs or improvements to be tested with time and solved.


## Requirements

For development, you will need Node.js, MongoDb and Postman installed on your environement.

#### Node and MongoDB installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node
    brew tap mongodb/brewbrew install mongodb-community@4.2
    mongod --config /usr/local/etc/mongod.conf

#### Node and MongoDB installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs
    sudo apt-get install -y mongodb-org

#### Node and MongoDB installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Just go on [official MongoDB website](https://treehouse.github.io/installation-guides/windows/mongo-windows.html) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

#### Postman installation on Mac, Linux and Windows
Just go on [official Postman website](https://www.getpostman.com) & grab the installer.

---

## Install

    $ git clone https://gitlab.com/omar-alavedra/clients-and-policies.git
    $ cd clients-and-policies
    $ npm install

## Start & watch (development)

1) Initialize MongoDb
    
    MAC -> $ mongo
        
    Linux -> $ sudo service mongod start
    
2) On another terminal tab and in the same directory as the downloaded repo:
    
    $ npm run start

3) Interact with the API using Postman (examples)
    - [Login first](https://assesment-doc-images.s3.eu-west-3.amazonaws.com/Screenshot+2019-09-23+at+15.57.01.png) with any user of the database, and the **default user password: myfirstLogin** to be able to get the token and use it for the next requests.
    - [Get client by id](https://assesment-doc-images.s3.eu-west-3.amazonaws.com/Screenshot+2019-09-23+at+15.57.19.png) after log in, include the generated token in the header as in the example.
    - [Get client by name](https://assesment-doc-images.s3.eu-west-3.amazonaws.com/Screenshot+2019-09-23+at+16.02.11.png) after log in, include the generated token in the header as in the example.
    - [Get the list of policies by user name](https://assesment-doc-images.s3.eu-west-3.amazonaws.com/Screenshot+2019-09-23+at+15.57.29.png) after log in, include the generated token in the header as in the example.
    - [Get the user linked to a policy number](https://assesment-doc-images.s3.eu-west-3.amazonaws.com/Screenshot+2019-09-23+at+16.01.38.png) after log in, include the generated token in the header as in the example.

## Test

    $ npm test
    
## Languages & tools

### JavaScript

- [bcrypt](https://www.npmjs.com/package/bcrypt)  is a password hashing function.
- [express](http://expressjs.com) is a  web application framework that provides a robust set of features for web and mobile applications.
- [joi](https://www.npmjs.com/package/joi) allows you to create blueprints or schemas for JavaScript objects (an object that stores information) to ensure validation of key information.
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) is an Internet standard for creating JSON-based access tokens that assert some number of claims.
- [mongodb](https://www.mongodb.com) is a general purpose, document-based, distributed database built for modern application developers.
- [mongoose](https://mongoosejs.com) provides a straight-forward, schema-based solution to model your application data.

### Testing tools for Javascript

- [chai](https://www.chaijs.com) is a BDD / TDD assertion library for node and the browser that can be delightfully paired with any javascript testing framework.
- [chai-http](https://www.chaijs.com/plugins/chai-http/) provides an interface for live integration testing via superagent.
- [mocha](https://mochajs.org) is a feature-rich JavaScript test framework running on Node.js and in the browser, making asynchronous testing simple.

