var chai = require('chai'),
chaiHttp = require('chai-http');

chai.use(chaiHttp);
var expect = require("chai").expect;

describe("User request tests", function() {
    var userCredentials;

    before(function(done){
        var request = require("request");

        var userOptions = {
            url: 'http://localhost:8080/login',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: 'userTest@user.com',
                password: 'myfirstLogin',
            }),
            method: 'PUT'
        };

        request(userOptions, function(err, res, body){
            body = JSON.parse(body)
            userCredentials = {
                id: body.id,
                token: body.token
            }
            done();
        })
    });
    
    describe("Get the clients requested data as user", function() {
        
        it("Lists user data filtered by user id", function(done) {
            chai.request('http://localhost:8080/api/clients/id')
                .get('/a0ece5db-cd14-4f21-812f-966633e7be86')
                .set('Authorization', 'Bearer ' + userCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    delete res.data._id
                    delete res.data.password
                    expect(res.type).to.equal("Data")
                    expect(res.data).to.deep.equal({
                            "__v": 0,
                            "email": "britneyblankenship@quotezart.com",
                            "id": "a0ece5db-cd14-4f21-812f-966633e7be86",
                            "name": "Britney",
                            "role": [
                                "admin"
                            ]
                        })
                    done()
                });
        })

        it("Lists user data filtered by user name", function(done) {
            chai.request('http://localhost:8080/api/clients/name')
                .get('/Manning')
                .set('Authorization', 'Bearer ' + userCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    delete res.data._id
                    delete res.data.password
                    expect(res.type).to.equal("Data")
                    expect(res.data).to.deep.equal({
                        "__v": 0,
                        "email": "manningblankenship@quotezart.com",
                        "id": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
                        "name": "Manning",
                        "role": [
                            "admin"
                        ]
                    })
                    done()
                });
        })

        it("Not able to access to all policies data", function(done) {
            chai.request('http://localhost:8080/api/policies')
                .get('/list')
                .set('Authorization', 'Bearer ' + userCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    expect(res.message).to.equal("Unauthorized")
                    done()
                });

        })

        it("Not able to access the list of policies linked to a username", function(done) {
            chai.request('http://localhost:8080/api/policies/name')
                .get('/Britney')
                .set('Authorization', 'Bearer ' + userCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    expect(res.message).to.equal("Unauthorized")
                    done()
                });

        })
        it("Not able to access the user linked to a policy number", function(done) {
            chai.request('http://localhost:8080/api/policies/id')
                .get('/5a72ae47-d077-4f74-9166-56a6577e31b9')
                .set('Authorization', 'Bearer ' + userCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    expect(res.message).to.equal("Unauthorized")
                    done()
                });

        })

    })
});

