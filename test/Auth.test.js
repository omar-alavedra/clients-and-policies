var chai = require('chai'),
chaiHttp = require('chai-http');

chai.use(chaiHttp);
var expect = require("chai").expect;

describe("User login tests", function() {
    it("Log in of a user from the clients database", function(done) {
          chai.request('http://localhost:8080')
            .put('/login')
            .type('form')
            .send({
                "email":"adminTest@admin.com",
                "password":"myfirstLogin"
            })
            .end(function (err, res) {
                res = res.body
                expect(res.message).to.equal("Login success.")
                expect(res.id).to.not.be.null
                expect(res.token).to.not.be.null
                done()
            });
    });

    it("Try to log in a non-existing user", function(done) {
        chai.request('http://localhost:8080')
          .put('/login')
          .type('form')
          .send({
              "email":"nonExistingUser@none.com",
              "password":"myfirstLogin"
          })
          .end(function (err, res) {
              res = res.body
              expect(res.message).to.equal("User does not exist.")
              done()
          });
    });

    it("Try to log in a user with wrong password", function(done) {
        chai.request('http://localhost:8080')
          .put('/login')
          .type('form')
          .send({
              "email":"adminTest@admin.com",
              "password":"invalidPassword"
          })
          .end(function (err, res) {
              res = res.body
              expect(res.message).to.equal("Login not successful. Invalid password.")
              done()
          });
    });
    
});

