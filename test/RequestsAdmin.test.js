var chai = require('chai'),
chaiHttp = require('chai-http');

chai.use(chaiHttp);
var expect = require("chai").expect;

describe("Admin request tests", function() {
    var adminCredentials;

    before(function(done){
        var request = require("request");

        var adminOptions = {
            url: 'http://localhost:8080/login',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: 'adminTest@admin.com',
                password: 'myfirstLogin',
            }),
            method: 'PUT'
        };
        request(adminOptions, function(err, res, body){
            body = JSON.parse(body)
            adminCredentials = {
                id: body.id,
                token: body.token
            }
            done();
        })
    });
    describe("Get the clients requested data as admin", function() {
        it("Lists all user data", function(done) {
            chai.request('http://localhost:8080/api/clients')
                .get('/list')
                .set('Authorization', 'Bearer ' + adminCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    expect(res.type).to.equal("Data")
                    done()
                });

        })
        it("Lists user data filtered by user id", function(done) {
            chai.request('http://localhost:8080/api/clients/id')
                .get('/a0ece5db-cd14-4f21-812f-966633e7be86')
                .set('Authorization', 'Bearer ' + adminCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    delete res.data._id
                    delete res.data.password
                    expect(res.type).to.equal("Data")
                    expect(res.data).to.deep.equal({
                            "__v": 0,
                            "email": "britneyblankenship@quotezart.com",
                            "id": "a0ece5db-cd14-4f21-812f-966633e7be86",
                            "name": "Britney",
                            "role": [
                                "admin"
                            ]
                        })
                    done()
                });

        })

        it("Lists user data filtered by user name", function(done) {
            chai.request('http://localhost:8080/api/clients/name')
                .get('/Manning')
                .set('Authorization', 'Bearer ' + adminCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    delete res.data._id
                    delete res.data.password
                    expect(res.type).to.equal("Data")
                    expect(res.data).to.deep.equal({
                        "__v": 0,
                        "email": "manningblankenship@quotezart.com",
                        "id": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
                        "name": "Manning",
                        "role": [
                            "admin"
                        ]
                    })
                    done()
                });

        })
        it("Lists all policies data", function(done) {
            chai.request('http://localhost:8080/api/policies')
                .get('/list')
                .set('Authorization', 'Bearer ' + adminCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    expect(res.type).to.equal("Data")
                    done()
                });
        })
        it("Lists of policies linked to a username", function(done) {
            chai.request('http://localhost:8080/api/policies/name')
                .get('/Britney')
                .set('Authorization', 'Bearer ' + adminCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    expect(res.type).to.equal("Data")
                    done()
                });

        })

        it("Lists the user linked to a policy number", function(done) {
            chai.request('http://localhost:8080/api/policies/id')
                .get('/5a72ae47-d077-4f74-9166-56a6577e31b9')
                .set('Authorization', 'Bearer ' + adminCredentials.token)
                .type('form')
                .end(function (err, res) {
                    res = res.body
                    delete res.data[0]._id
                    delete res.data[0].password
                    expect(res.type).to.equal("Data")
                    expect(res.data[0]).to.deep.equal(
                        {
                          role: [ 'admin' ],
                          id: 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb',
                          name: 'Manning',
                          email: 'manningblankenship@quotezart.com',
                          __v: 0
                        }
                      )
                    done()
                });

        })

    })

});

