var express = require('express'),
app = express(),
port = process.env.PORT || 8080,
bodyParser = require('body-parser');

//Database configuration with mongodb and mongoose
const database = require('./api/config/db');
database();

const startup = require('./api/startup/startup');
startup.initializeClients();
startup.initializePolicies();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/backendRoutes'); //importing route
routes(app); //register the route

app.listen(port);

console.log('backend AXA RESTful API server started on: ' + port);
