'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/** Schema for a db representing the policy. */
const policySchema = new Schema({
    id: String,
    amountInsured: Number,
    email: String,
    inceptionDate: String,
    installmentPayment: Boolean,
    clientId: String
});

module.exports = mongoose.model('Policies', policySchema);
