'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/** Schema for a db representing the client. */
const clientsSchema = new Schema({
    id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
        minlenght: 5,
        maxlength: 255,
    },
    role: {
        type: [{
          type: String,
          enum: ['user', 'admin']
        }],
        default: 'user'
    } 
});

const Client = mongoose.model('Clients', clientsSchema);
module.exports = Client; 
