'use strict';

const policyService = require('../services/policiesService')

/** Function representing all the different routes of the API requests. */
module.exports = function(app) {
    var clientsList = require('../controllers/clientsController');
    var policiesList = require('../controllers/policiesController');
    var authController = require('../controllers/authController');

    app.all("/api/*",policyService.validateToken,function(req,res,next){
        next()
    })
    app.all("/api/clients/*",policyService.userOrAdmin, function(req,res,next){
        next()
    })
    app.route('/login')
        .put(authController.login)

    //Client Routes
    app.route('/api/clients/list')
        .get(clientsList.listClients)

    app.route('/api/clients/id/:clientId')
        .get(clientsList.listClientId)

    app.route('/api/clients/name/:clientName')
        .get(clientsList.listClientUsername)

    //Policy routes
    app.all("/api/policies/*",policyService.onlyAdmin, function(req,res,next){
        next()
    })
    app.route('/api/policies/list')
        .get(policiesList.listPolicies)

    app.route('/api/policies/name/:clientName')
        .get(policiesList.listUserPolicies)
        
    app.route('/api/policies/id/:policyNumber')
        .get(policiesList.listUser)
};