'use strict';

const clientSchema = require('../models/clientModel');

/** Function representing the List controller for display of all the clients. */
exports.listClients = async function(req, res) {
    const users =  await clientSchema.find();
    if(users.length === 0) {
        res.send({type: "Error", message: "clients not found"})
    }
    else res.send({type: "Data", data: users})
};

/** Function representing the List client by ID controller for the clients. */
exports.listClientId = async function(req, res) {
    const id = req.params.clientId;
    const user =  await clientSchema.find({id: id});

    if(user.length === 0) {
        res.send({type: "Error", message: "client not found"})
    }
    res.send({type: "Data", data: user[0]})

};

/** Function representing the List Client by UserName controller for the clients. */
exports.listClientUsername = async function(req, res) {
    const name = req.params.clientName;
    const user =  await clientSchema.find({name: name});

    if(user.length === 0) {
        res.send({type: "Error", message: "client not found"})
    }
    res.send({type: "Data", data: user[0]})    
};
