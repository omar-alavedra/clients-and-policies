'use strict';

const policySchema = require('../models/policyModel');
const clientSchema = require('../models/clientModel');

/** Function representing the List controller for display of all the policies. */
exports.listPolicies = async function(req, res) {
    const policies =  await policySchema.find();
    if(policies.length === 0) {
        res.send({type: "Error", message: "clients not found"})
    }
    res.send({type: "Data", data: policies})
};

/** Function representing the List of policies linked to a username policies controller. */
exports.listUserPolicies = async function(req, res) {
    const clientName = req.params.clientName.toLowerCase();
    const name = clientName.charAt(0).toUpperCase() + clientName.slice(1)
    const user =  await clientSchema.find({name: name});

    if(user.length === 0) {
        res.send({type: "Error", message: "client not found"})
    }
    else {
        const userId = user[0].id
        const policies =  await policySchema.find({clientId: userId});

        if(policies.length === 0){
            res.send({type: "Message", message: "No policies related to this user."})
        }
        else{
            res.send({type: "Data", data: policies})
        }
    }
};

/** Function representing the List user linked to a policy controller. */
exports.listUser = async function(req, res) {
    const policyId = req.params.policyNumber;
    const policy =  await policySchema.find({id: policyId});

    if(policy.length === 0) {
        res.send({type: "Error", message: "Policy not found"})
    }
    else {
        const userId = policy[0].clientId;
        const user =  await clientSchema.find({id: userId});
        if(user.length === 0){
            res.send({type: "Message", message: "No users related to this user."})
        }
        else{
            res.send({type: "Data", data: user})
        }
    }
};