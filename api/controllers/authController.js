'use strict';
const clientSchema = require('../models/clientModel');
const bcrypt = require('bcrypt');
const jwt = require('../services/jwtService');

/** Function representing the Login controller for the clients. */
exports.login = async function(req, res) {
    const email = req.body.email;
    const user =  await clientSchema.find({email: email});
    // console.log(user)
    if(user.length>0){
        bcrypt.compare(req.body.password,user[0].password,function(err,response){
            // console.log(err)
            if(response===true){
                const token = jwt.issueToken({id:user[0].id})
                res.send({type: "Message", message: "Login success.", id:user[0].id, token})
            }
            else{
                res.send({type: "Error", message: "Login not successful. Invalid password."})
            }
        })
    }
    else{
        res.send({type: "Error", message: "User does not exist."})
    }
    
};