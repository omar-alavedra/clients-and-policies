const jwt = require('../services/jwtService');
const clientSchema = require('../models/clientModel');

/** Function that verifies a token given from a user. */
exports.validateToken = function(req, res, next) {
    if(req.headers.authorization){
        const authorization = req.headers.authorization.split(" ")
        if(authorization[0] && authorization[0]==="Bearer"){
            if(authorization[1]){
                jwt.verifyToken(authorization[1],function(err, authorized){
                    if (err) res.send({type: "error", message: "Unauthorized"})
                       else next()
                })
            } 
        }
    }
    else {
        res.send({type: "error", message: "Unauthorized"})
    }
    
};

/** Function that checks if the user is Admin. */
exports.onlyAdmin = function(req,res,next) {
    if(req.headers){
        const authorization = req.headers.authorization.split(" ")
        if(authorization[0] && authorization[0]==="Bearer"){
            if(authorization[1]){
                jwt.verifyToken(authorization[1],async function(err, authorized){
                    if(authorized){
                        const users =  await clientSchema.find({id:authorized.id});
                        if(users.length>0 && users[0].role[0] === "admin"){
                            next()
                        }
                        else{
                            res.send({type: "error", message: "Unauthorized"})
                        }
                    }
                })
            } 
        }
    }
    else {
        res.send({type: "error", message: "Unauthorized"})
    }
}

/** Function that checks if the user is Admin or User. */
exports.userOrAdmin = function(req,res,next) {
    if(req.headers){
        const authorization = req.headers.authorization.split(" ")
        if(authorization[0] && authorization[0]==="Bearer"){
            if(authorization[1]){
                jwt.verifyToken(authorization[1],async function(err, authorized){
                    if(authorized){
                        const users =  await clientSchema.find({id:authorized.id});
                        if(users.length>0 && (users[0].role[0] === "user" || users[0].role[0] === "admin")){
                            next()
                        }
                        else{
                            res.send({type: "error", message: "Unauthorized"})
                        }
                    }
                })
            } 
        }
    }
    else {
        res.send({type: "error", message: "Unauthorized"})
    }
    
}