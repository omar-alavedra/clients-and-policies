const jwt = require('jsonwebtoken');
const secret = "thisIsMySecretForTheBackend";

/** Function that issues a token for the login of a user. */
module.exports.issueToken = function(payload) {
    var token =  jwt.sign(payload, secret, {expiresIn: '100d'});
    return token;
}

/** Function that verifies a token given from a user. */
module.exports.verifyToken = function(token, callback) {
    return jwt.verify(token, secret, callback)
}