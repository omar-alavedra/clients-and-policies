// mongoose instance connection url connection
const mongoose = require("mongoose");
const MONGO_HOSTNAME = '127.0.0.1';
const MONGO_PORT = '27017';
const MONGO_DB = 'clients';

/** Function representing the database configuration and connection with mongoDB. */
module.exports = function(req, res) {
    module.exports = mongoose;
    mongoose.Promise = global.Promise;
    const url = `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;
    try {
        mongoose.set('useCreateIndex', true),
        mongoose.connect(url,{useNewUrlParser: true, useUnifiedTopology: true })
        mongoose.connection.dropDatabase()
    } catch(error) {
        console.log(`> error connecting to the database: ${error}`);
    }
    finally {
        console.log("> successfully opened the database");
    }
};