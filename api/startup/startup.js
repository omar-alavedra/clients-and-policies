const Policies = require('../statics/policies');
const Clients = require('../statics/clients');
const clientSchema = require('../models/clientModel');
const policieSchema = require('../models/policyModel');
const bcrypt = require('bcrypt');

/** Function that initializes the clients in the database from a given JSON. */
exports.initializeClients = function(req, res) {
    Clients.clients.forEach(client => {
        bcrypt.hash("myfirstLogin",10,function(err, hash){
            let Client = new clientSchema({
                id: client.id,
                name: client.name,
                email: client.email,
                password: hash,
                role:  client.role,
            })
            Client.save(function (err) {
                if (err) return console.error(err);
                // console.log(Client.name + " saved to clients collection.");
            });
        })
        
    });
};

/** Function that initializes the policies in the database from a given JSON. */
exports.initializePolicies = function(req, res) {
    Policies.policies.forEach(policy => {
        let Policy = new policieSchema({
            id: policy.id,
            amountInsured: policy.amountInsured,
            email: policy.email,
            inceptionDate: policy.inceptionDate,
            installmentPayment: policy.installmentPayment,
            clientId: policy.clientId
        })
        Policy.save(function (err) {
            if (err) return console.error(err);
            // console.log(Policy.id + " saved to policies collection.");
        });
    });
};